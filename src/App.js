import "./style/App.css";
import GoogleFontLoader from "react-google-font-loader";
import React, { useReducer } from "react";
import Groups from "./components/Groups";
import "bootstrap/dist/css/bootstrap.min.css";

export const context = React.createContext();

const App = () => {
    const reducer = (state, action) => {
        /* eslint-disable indent */
        switch (action.type) {
            case "SET_LOADING":
                return { ...state, loading: true };
            case "SET_GROUPS":
                return { ...state, loading: false, groups: action.payload };
            default:
                return state;
        }
        /* eslint-enable indent */
    };

    const [state, dispatch] = useReducer(reducer, {
        loading: true,
        groups: [],
    });

    return (
        <context.Provider value={{ state, dispatch }}>
            <GoogleFontLoader
                fonts={[
                    {
                        font: "Bakbak One",
                        weights: [400],
                    },
                ]}
            />
            <section id="bigSection">
                <h1 style={{ fontFamily: "Bakbak One" }}>
                    MHA Task App - v3.0
                </h1>
                <Groups />
            </section>
        </context.Provider>
    );
};

export default App;
