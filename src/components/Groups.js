import { useEffect, useContext } from "react";
import { context } from "../App";
import Group from "./Group";
import "../style/Groups.css";
import { Button } from "react-bootstrap";

const Groups = () => {
    const { state, dispatch } = useContext(context);
    const fetchData = async () => {
        const result = await fetch("http://localhost:5000/groups");
        const groups = await result.json();
        dispatch({ type: "SET_GROUPS", payload: groups });
    };

    useEffect(() => {
        dispatch({ type: "SET_LOADING" });
        fetchData();
    }, []);

    return (
        <section id="content">
            {state.groups.map((group) => {
                return <Group key={group.id} group={group} />;
            })}
            <Button variant="primary" size="lg" style={{ margin: "15px" }}>
                Create new group
            </Button>
        </section>
    );
};

export default Groups;
