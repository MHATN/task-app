import { Card } from "react-bootstrap";
import styles from "../style/Group.module.css";
import PropTypes from "prop-types";

const Group = ({ group }) => {
    return (
        <Card className={styles.card} data-testid={group.id}>
            <Card.Body
                className={styles["card-body"]}
                style={{ borderLeftColor: group.color }}
            >
                <div>
                    <Card.Title>{group.title}</Card.Title>
                    <Card.Text>{group.number} tasks</Card.Text>
                </div>
            </Card.Body>
        </Card>
    );
};

Group.propTypes = {
    group: PropTypes.object,
};

Group.defaultProps = {
    group: {
        id: 0,
        title: "Error",
        number: 0,
        color: "black",
    },
};

export default Group;
