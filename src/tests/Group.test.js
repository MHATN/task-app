import Group from "../components/Group";
import "@testing-library/jest-dom";
import { render, cleanup, screen } from "@testing-library/react";
import TestRenderer from "react-test-renderer";

afterEach(() => {
    cleanup();
});

describe("Unit tests", () => {
    describe("React tests", () => {
        describe("Group element", () => {
            test("Render a group", () => {
                const group = {
                    id: 1,
                    title: "General",
                    color: "red",
                    number: 3,
                };
                render(<Group group={group} />);
                const groupElement = screen.getByTestId(group.id);
                expect(groupElement).toBeInTheDocument();
                expect(groupElement).toHaveTextContent(
                    `${group.title}${group.number} tasks`
                );
                expect(groupElement.querySelector(".card-body")).toHaveStyle({
                    borderLeftColor: group.color,
                });
            });

            test("Render a group without props", () => {
                render(<Group />);
                const groupElement = screen.getByTestId(0);
                expect(groupElement).toBeInTheDocument();
                expect(groupElement).toHaveTextContent("Error0 tasks");
                expect(groupElement.querySelector(".card-body")).toHaveStyle({
                    borderLeftColor: "black",
                });
            });

            test("Matches snapshot of Group", () => {
                const tree = TestRenderer.create(<Group />).toJSON();
                expect(tree).toMatchSnapshot();
            });
        });
    });
});
