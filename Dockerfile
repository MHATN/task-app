FROM node:18.12.1

WORKDIR /app

COPY ["yarn.lock", "package.json", "./"]

RUN yarn install

COPY . .

RUN yarn run build

EXPOSE 3000

CMD ["yarn", "run", "serve"]